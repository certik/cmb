/*=========================================================================

Copyright (c) 1998-2005 Kitware Inc. 28 Corporate Drive, Suite 204,
Clifton Park, NY, 12065, USA.

All rights reserved. No part of this software may be reproduced,
distributed,
or modified, in any form or by any means, without permission in writing from
Kitware Inc.

IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY FOR
DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT
OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY DERIVATIVES THEREOF,
EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
INCLUDING,
BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE IS PROVIDED ON AN
"AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE NO OBLIGATION TO
PROVIDE
MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

=========================================================================*/

#ifndef _pqCMBTabWidgetTranslator_h
#define _pqCMBTabWidgetTranslator_h

#include "pqWidgetEventTranslator.h"
#include "vtkCMBClientModule.h" // For export macro
#include <QPointer>
class QTabBar;

/**
Translates low-level Qt events into high-level ParaView events that can be recorded as test cases.

\sa pqEventTranslator
*/

class VTKCMBCLIENT_EXPORT pqCMBTabWidgetTranslator :
  public pqWidgetEventTranslator
{
  Q_OBJECT

public:
  pqCMBTabWidgetTranslator(QObject* p=0);

  virtual bool translateEvent(QObject* Object, QEvent* Event, bool& Error);

protected slots:
  void indexChanged(int);

private:
  pqCMBTabWidgetTranslator(const pqCMBTabWidgetTranslator&);
  pqCMBTabWidgetTranslator& operator=(const pqCMBTabWidgetTranslator&);

  QPointer<QTabBar> CurrentObject;

};

#endif // !_pqCMBTabWidgetTranslator_h
